import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register() {

	// to access the "user" state from "UserContext"
	const {user} = useContext(UserContext);

	// State hooks to store values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	const navigate = useNavigate();

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	
	// Checks the email if it does not exist and then registers the user
	function checkEmailExists(e){

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			// JSON.stringify converts object data into stringified JSON
			body: JSON.stringify({
				email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				registerUser(data); //pass the result "data" to register user for further validation
			} else {
				Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					confirmButtonColor: "#0d6efd",
					text: 'Please try again'
				});
			}
		}).catch(error => {
			Swal.fire({
				title: 'Something went wrong!',
				icon: 'error',
				confirmButtonColor: "#0d6efd",
				text: 'Please try again'
			});
		});;

	}

	// Registers the user
	function registerUser(data){
		//checks if data is true or false
		if(data === false){
			fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				// JSON.stringify converts object data into stringified JSON
				body: JSON.stringify({
					firstName,
					lastName,
					email,
					mobileNo,
					password: password2
				})
			})
			.then(res => res.json())
			.then(data => {

				if(data){
					Swal.fire({
						title: 'Registration Successful',
						icon: 'success',
						confirmButtonColor: "#0d6efd",
						text: 'Welcome to Zuitt!'
					});
					navigate('/login');
				} else {
					Swal.fire({
						title: 'Something went wrong!',
						icon: 'error',
						confirmButtonColor: "#0d6efd",
						text: 'Please try again'
					});
				}				
			}).catch(error => {
				Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					confirmButtonColor: "#0d6efd",
					text: 'Please try again'
				});
			});;;
		} else {
			Swal.fire({
				title: 'Duplicate Email Found!',
				icon: 'error',
				confirmButtonColor: "#0d6efd",
				text: 'Please provide a different email'
			});
		}
	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match

		if((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (password1.length && password2.length >= 8) && (mobileNo.length >= 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password1, password2]);
	
	return (

		// if the user already logged in, they will be redirected to the courses page
		(user.id !== null) ?
			<Navigate to="/courses" />
			
		:
		<Form onSubmit={(e) => checkEmailExists(e)}>
			<h1>Register</h1>
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter first name here"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter last name here"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>
			
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter mobile number here"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					Must be 8 characters or more.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ? 

				<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>
				:
				<Button variant="secondary my-3" type="submit" id="submitBtn" disabled>Submit</Button>

			}
			

		</Form>
	)
}