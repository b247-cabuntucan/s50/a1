import CourseCard from '../components/CourseCard'
import { useEffect, useState } from 'react';

export default function Courses() {
    // console.log(coursesData);
    // console.log(coursesData[0]);

    // The "course" in the CourseCard component is called a "Prop" which is a shorthand for "property" since component are considered...


    // The map method loops through the individual course object in out array and returns a component for each courses
    // const courses = coursesData.map(course => {
    //     return (
    //         <CourseCard key={course.id} course={course}/>
    //     )
    // })


    // State that will be used to store the courses retrieved from the database 
    const [courses, setCourses] = useState([]);

    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
        .then(res => res.json())
        .then(data => {

            // Sets the "courses" state to map the data retrieved from the fetch request into several
            setCourses(data.map(course => {
                return(
                    <CourseCard key={course._id} course={course} />
                )
            }))
        })
    })

    return (
        <>
            {courses}
        </>
    )
} 
