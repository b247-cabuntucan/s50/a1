// [ACTIVITY S53 Output]
//import { Button } from "react-bootstrap"
// import { Link } from "react-router-dom"
// export default function NotFound() {
//   return (
//     <div className="text-center">
//       <h1>Sorry... The page you are looking for does not exist!</h1>
//       <p>404 Not Found</p>
//       <Button variant="warning" as={Link} to="/">Go Back Home</Button>
//       </div>
//   )
// }

import Banner from "../components/Banner";

export default function Error(){

  const data = {
    title: "Error 404 - Page Not Found!",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back to Home"
  }

  return (
    <Banner data={data} />
  )
}