import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';

import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Login() {

	// Allows us to consume the User Context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	//This hook return a function that lets you navigate to components
	// const navigate = useNavigate();
	// State hooks to store values of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Login logic function
	function authenticate(e){

		e.preventDefault();
		//connect to our api
		//if process.env doesn't work, just restart the dev server
		// NOTES: "fetch" method is used to send request in the server and loa the received in the webpage
		
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			// JSON.stringify converts object data into stringified JSON
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		// The first ".then" method from the "response" object to convert the data retrieved into JSON format to be used in our application
		.then(res => res.json())
		.then(data => {
			// We will receive either a token or an error response
			console.log(data)

			// If no user information is found, the "access" property will not be available and will return undefined
			// Using the "typeof" operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
			if(typeof data.access !== "undefined"){
				// The JWT will be used to retrieved user information across the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access); //from the retrieveUserDetails function defined below

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					confirmButtonColor: "#0d6efd",
					text: 'Welcome to Zuitt!'
				})
			} else {
				Swal.fire({
					title: 'Authetication Failed!',
					icon: 'error',
					confirmButtonColor: "#0d6efd",
					text: 'Please, check you login details and try again!'
				})
			}
		});

		// Set the email of the authenticated user in the local storage

		// localStorage.setItem("email", email)
		// setUser({email: localStorage.getItem('email')});

		setEmail("");
		setPassword("");
		// navigate('/');

		// alert("You are now logged in!");
	}

	const retrieveUserDetails = (token) => {

		// The token will be sent as part of the request's header information
		// We put "Bearer" in front of the token to follow implementation standards for JWTs
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			//Global user state for validation across the whole app
			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		});
	}


	useEffect(() => {
		//checks if email is in the correct format (using Regex)
		//the "Login" button will only be active if the conditions are met
		if((email !== "" && password !== "") && (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	
	return (
		(user.id !== null) ?
			<Navigate to="/courses" />
			
		:

		<Form onSubmit={(e) => authenticate(e)}>
			<h1>Login</h1>
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			
			</Form.Group>

			{ isActive ? 

				<Button variant="success my-3" type="submit" id="submitBtn">Login</Button>
				:
				<Button variant="secondary my-3" type="submit" id="submitBtn" disabled>Login</Button>

			}
			

		</Form>
	)
}