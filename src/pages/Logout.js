import { useContext, useEffect } from "react"
import { Navigate } from "react-router-dom"
import UserContext from "../userContext"

export default function Logout() {
    localStorage.clear()

    const {unsetUser, setUser} = useContext(UserContext);

    unsetUser();

    //Placing 
    useEffect(() => {
      setUser({id: null})
    });
    
  return (
    <Navigate to="/login" />
  )
}
