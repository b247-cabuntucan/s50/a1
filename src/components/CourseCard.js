import { Card, Button } from 'react-bootstrap'
// import { useState, useEffect } from 'react';
import PropTypes from 'prop-types'

import { Link } from 'react-router-dom';

export default function CourseCard({course}) {

    //Destructure the "course" properties into their own variables "course" to make the code even shorter
    const {name, description, price, _id} = course;

    // use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components

    // Syntax 
        // const [getter, setter] = useState(initialGetterValue)

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30);
    // const [isOpen, setIsOpen] = useState(true);

    // function enroll() {
    //     if(seats > 0) {
    //         setCount(count + 1);
    //         setSeats(seats - 1);
    //         console.log("Seats: " + seats);
            
    //     } else {
    //         alert("No more seats available!")
    //     }
    // }

    // function enroll(){
    //     setCount(count + 1);
    //     setSeats(seats - 1);
    // }

    // //useEffect - allows use to instruct the app that the compnents needs to do something after render.
    // useEffect(() => {
    //   if (seats === 0 ){
    //     setIsOpen(false);
    //     document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
    //     alert("No more seats available.")
    //   }
    //     //will run anytime one of the values in the array of dependencies changes.
    // }, [seats]);
    
    
    return (
        <Card className="cardHighlight p-3 my-4">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Button className='bg-primary' as={Link} to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    );
}

// Check if the CourseCard component is getting the correct prop types
// PropTypes are used for validation information passed to a component and is a tool normally used to help developers ensure the corret information is passed from one components to the next.
CourseCard.propTypes = {
    course: PropTypes.shape({
        // Define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
}