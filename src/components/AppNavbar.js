
import { useContext, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import {Link, NavLink} from 'react-router-dom';
import UserContext from '../userContext';

export default function AppNavBar() {

  // const [user, setUser] = useState(localStorage.getItem('email'));

  const {user} = useContext(UserContext);
  console.log(user);

  return (
    

    <Container fluid>
    <Navbar bg="light" expand="sm">
                  {/* "as={Link}" attribute will change the current tag to the "Link" component*/}
        <Navbar.Brand as={Link} to='/'>Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
                <Nav.Link as={Link} to='/'>Home</Nav.Link>
                <Nav.Link as={Link} to='/courses'>Courses</Nav.Link>

              {(user.id !== null) ?
                <Nav.Link as={Link} to='/logout'>Logout</Nav.Link>
                :
                <>
                <Nav.Link as={Link} to='/login'>Login</Nav.Link>
                <Nav.Link as={Link} to='/register'>Register</Nav.Link>
                </>
              }
            </Nav>
        </Navbar.Collapse>
    </Navbar>
    </Container>
  );
}
