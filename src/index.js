import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';
//import the bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// // const name = "John Smith";
// // const element = <h1>Hello, {name}</h1>

// // root.render(element);

// const user = {
//   firstName: 'Jane',
//   lastName: 'Smith'
// }

// function formatName(name) {
//   return user.firstName + " " + user.lastName;
// };

// const element = <h1>Hello, {formatName(user)}</h1>
// root.render(element);