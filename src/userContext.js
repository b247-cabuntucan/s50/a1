import React from 'react';

//initializes a React Context
// Creates a context object
// A context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the app
// The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop-drilling
    //Prop-Drilling, is the term we use wheneever we pass information from one component form one component to another user props
const UserContext = React.createContext();

//initializes a Context Provider
// Give the ability to provide specific context through components
// The 'Provider' components allows other components to consume/use/utilize/facilitate context obeject and supply the necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;