# S50-S55 React Application Discussion/Activity

### S50 Intro to React

install

`npx create-react-app react-app` to create react app with project name react-app

`cd react-app` 

`npm start` to start the server

Remove such files:

> App.test.js

> index.css

> logo.svg

> reportWebServices.js

Added activity code s50 = CourseCard component

### S51 **Props and States**

### S52 **Effect Hook and Forms**

### S53 ***React Router***

install react-router-dom

`npm install react-router-dom`

### S54 ***React State Management***